import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestimonialPageRoutingModule } from './testimonial-page-routing.module';
import { ListTestimonialsComponent } from './components/list-testimonials/list-testimonials.component';
import { ViewTestimonialModalComponent } from './components/view-testimonial-modal/view-testimonial-modal.component';


@NgModule({
  declarations: [
    ListTestimonialsComponent,
    ViewTestimonialModalComponent
  ],
  imports: [
    CommonModule,
    TestimonialPageRoutingModule
  ]
})
export class TestimonialPageModule { }
