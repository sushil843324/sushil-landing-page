import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListTestimonialsComponent } from './components/list-testimonials/list-testimonials.component';

const routes: Routes = [
  {
    path : "",
    component : ListTestimonialsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestimonialPageRoutingModule { }
