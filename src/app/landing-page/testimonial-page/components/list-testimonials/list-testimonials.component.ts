import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LandingPageService } from 'src/app/landing-page/services/landing-page.service';
import { ViewTestimonialModalComponent } from '../view-testimonial-modal/view-testimonial-modal.component';

@Component({
  selector: 'app-list-testimonials',
  templateUrl: './list-testimonials.component.html',
  styleUrls: ['./list-testimonials.component.scss']
})
export class ListTestimonialsComponent implements OnInit {

  constructor(
    private service : LandingPageService,
    private modal : NgbModal
  ) { }

  testimonialList :  any
  isReadMore = false

  ngOnInit(): void {
    this.fetchAllTestimonials();
  }

  fetchAllTestimonials() {
    this.service.getAllTestimonials().subscribe((res) => {
      this.testimonialList = res.data;
    })
  }

  openModal(data:any) {
    const modal = this.modal.open(ViewTestimonialModalComponent)

    modal.componentInstance.testimonial = data; 
  }

}
