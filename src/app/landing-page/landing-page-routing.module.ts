import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './main-layout/main-layout.component';

const routes: Routes = [
  {
    path : "landing-page",
    component : MainLayoutComponent
  },
  {
    path : "pricing-page",
    loadChildren : () => import("../landing-page/pricing-page/pricing-page.module").then(
      (m) => m.PricingPageModule
    )
  },
  {
    path : "testimonial-page",
    loadChildren : () => import("../landing-page//testimonial-page/testimonial-page.module").then(
      (m) => m.TestimonialPageModule
    )
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingPageRoutingModule { }
