import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingPageRoutingModule } from './landing-page-routing.module';
import { LandingPageBannerComponent } from './components/landing-page-banner/landing-page-banner.component';
import { LandingPageFeaturesComponent } from './components/landing-page-features/landing-page-features.component';
import { LandingPageStepsComponent } from './components/landing-page-steps/landing-page-steps.component';
import { LandingPageCategoriesComponent } from './components/landing-page-categories/landing-page-categories.component';
import { LandingPagePricingComponent } from './components/landing-page-pricing/landing-page-pricing.component';
import { LandingPageTestimonialsComponent } from './components/landing-page-testimonials/landing-page-testimonials.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { LandingPageSlidingLogosComponent } from './components/landing-page-sliding-logos/landing-page-sliding-logos.component';
import { ViewTestimonialModalComponent } from './components/landing-page-testimonials/view-testimonial-modal/view-testimonial-modal.component';


@NgModule({
  declarations: [
    LandingPageBannerComponent,
    LandingPageFeaturesComponent,
    LandingPageStepsComponent,
    LandingPageCategoriesComponent,
    LandingPagePricingComponent,
    LandingPageTestimonialsComponent,
    MainLayoutComponent,
    LandingPageSlidingLogosComponent,
    ViewTestimonialModalComponent
  ],
  imports: [
    CommonModule,
    LandingPageRoutingModule,
    CarouselModule
  ]
})
export class LandingPageModule { }
