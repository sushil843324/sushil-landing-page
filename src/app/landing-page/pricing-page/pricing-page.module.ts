import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PricingPageRoutingModule } from './pricing-page-routing.module';
import { ListPricingCardsComponent } from './components/list-pricing-cards/list-pricing-cards.component';
import { ContactCardComponent } from './components/contact-card/contact-card.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ListPricingCardsComponent,
    ContactCardComponent,
    MainLayoutComponent
  ],
  imports: [
    CommonModule,
    PricingPageRoutingModule,
    ReactiveFormsModule
  ]
})
export class PricingPageModule { }
