import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPricingCardsComponent } from './list-pricing-cards.component';

describe('ListPricingCardsComponent', () => {
  let component: ListPricingCardsComponent;
  let fixture: ComponentFixture<ListPricingCardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListPricingCardsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListPricingCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
