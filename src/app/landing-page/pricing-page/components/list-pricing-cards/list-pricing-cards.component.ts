import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-pricing-cards',
  templateUrl: './list-pricing-cards.component.html',
  styleUrls: ['./list-pricing-cards.component.scss']
})
export class ListPricingCardsComponent implements OnInit {

  constructor() { }

  @Input() budgetList : any;

  ngOnInit(): void {
  }

}
