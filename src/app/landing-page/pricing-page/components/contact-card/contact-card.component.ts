import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LandingPageService } from 'src/app/landing-page/services/landing-page.service';

@Component({
  selector: 'app-contact-card',
  templateUrl: './contact-card.component.html',
  styleUrls: ['./contact-card.component.scss']
})
export class ContactCardComponent implements OnInit {

  constructor(
    private fb : FormBuilder,
    private service : LandingPageService
  ) { }

  form! : FormGroup

  ngOnInit(): void {
    this.form = this.fb.nonNullable.group({
      name : ["", [Validators.required]],
      email : ["", [Validators.required]],
      city : ["", [Validators.required]],
      phone : [, [Validators.required]],
      message : ["", [Validators.required]],
    })
  }


  onSubmit() {
    this.service.addSupportMessage(this.form.value).subscribe((res) => {
      console.log(res, "support message added!");
      this.form.reset();
    })
  }

}
