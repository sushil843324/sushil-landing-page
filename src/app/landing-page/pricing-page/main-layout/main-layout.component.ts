import { Component, OnInit } from '@angular/core';
import { LandingPageService } from '../../services/landing-page.service';
import { FormBuilder } from '@angular/forms';
import { combineLatest, map, startWith } from 'rxjs';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor(
    private service : LandingPageService,
    private fb : FormBuilder
  ) { }

  searchBarControl = this.fb.nonNullable.control("");


  ngOnInit(): void {
  }

  
  budgetList$ =   this.service.getBudgetList().pipe(
    map((res) => res.data)
  )

  filteredList$ = combineLatest([this.budgetList$, this.searchBarControl.valueChanges.pipe(startWith(""))]).pipe(
    map(([budgets, searchTerm]) => searchTerm.trim() === "" ? budgets: budgets.filter((budget : any) => new RegExp(searchTerm, "i").test(budget.role)))
  )



}
