import { Component, OnInit } from '@angular/core';
import { LandingPageService } from '../services/landing-page.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor(
    private service : LandingPageService
  ) { }

  
  
  bannerSectionData : any;
  slidingLogoSectionData : any
  featureSectionData : any;
  stepSectionData : any;
  jobCategorySectionData : any;
  pricingSectionData : any;
  testimonialSectionData : any;





  ngOnInit(): void {
    this.fetchFeatureSectionData();
    this.fetchSlidingLogoSectionData();
    this.fetchStepSectionData();
    this.fetchJobCategorySectionData();
    this.fetchPricingSectionData();
    this.fetchTestimonialSectionData();
    this.fetchBannerSectionData();
  }

  fetchFeatureSectionData() {
    this.service.getFeatureSectionData().subscribe((res) => {
      this.featureSectionData = res.data;
    })
  }

  fetchStepSectionData() {
    this.service.getStepSectionData().subscribe((res) => {
      this.stepSectionData = res.data;
    })
  }

  fetchJobCategorySectionData() {
    this.service.getJobCategorySectionData().subscribe((res) => {
      this.jobCategorySectionData = res.data;
    })
  }

  fetchPricingSectionData() {
    this.service.getPricingSectionData().subscribe((res) => {
      this.pricingSectionData = res.data;
    })
  }

  fetchTestimonialSectionData() {
    this.service.getTestimonialSectionData().subscribe((res) => {
      this.testimonialSectionData = res.data;
    })
  }

  fetchBannerSectionData() {
    this.service.getBannerSectionData().subscribe((res) => {
      this.bannerSectionData = res.data;
    })
  }

  fetchSlidingLogoSectionData() {
    this.service.getSlidingLogos().subscribe((res) => {
      this.slidingLogoSectionData = res.data;
    })
  }

}
