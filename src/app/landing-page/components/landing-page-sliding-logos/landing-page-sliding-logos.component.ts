import { Component, Input, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-landing-page-sliding-logos',
  templateUrl: './landing-page-sliding-logos.component.html',
  styleUrls: ['./landing-page-sliding-logos.component.scss']
})
export class LandingPageSlidingLogosComponent implements OnInit {

  constructor() { }

  @Input() slidingLogos : any

  ngOnInit(): void {
  }

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 1500,
    autoplay : true,
    navText: ['', ''],
    items : 3,
    margin : 10,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      // 940: {
      //   items: 4
      // }
    },
    nav: true
  }

}
