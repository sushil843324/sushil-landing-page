import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPageSlidingLogosComponent } from './landing-page-sliding-logos.component';

describe('LandingPageSlidingLogosComponent', () => {
  let component: LandingPageSlidingLogosComponent;
  let fixture: ComponentFixture<LandingPageSlidingLogosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingPageSlidingLogosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LandingPageSlidingLogosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
