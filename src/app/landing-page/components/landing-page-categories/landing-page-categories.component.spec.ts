import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPageCategoriesComponent } from './landing-page-categories.component';

describe('LandingPageCategoriesComponent', () => {
  let component: LandingPageCategoriesComponent;
  let fixture: ComponentFixture<LandingPageCategoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingPageCategoriesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LandingPageCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
