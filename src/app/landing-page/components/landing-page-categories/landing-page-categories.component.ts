import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page-categories',
  templateUrl: './landing-page-categories.component.html',
  styleUrls: ['./landing-page-categories.component.scss']
})
export class LandingPageCategoriesComponent implements OnInit {

  constructor() { }

  @Input() jobCategoryDetail : any

  ngOnInit(): void {
  }

}
