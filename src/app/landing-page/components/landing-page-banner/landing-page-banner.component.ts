import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page-banner',
  templateUrl: './landing-page-banner.component.html',
  styleUrls: ['./landing-page-banner.component.scss']
})
export class LandingPageBannerComponent implements OnInit {

  constructor() { }

  @Input() bannerDetail : any

  ngOnInit(): void {
  }

}
