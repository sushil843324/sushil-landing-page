import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPageTestimonialsComponent } from './landing-page-testimonials.component';

describe('LandingPageTestimonialsComponent', () => {
  let component: LandingPageTestimonialsComponent;
  let fixture: ComponentFixture<LandingPageTestimonialsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingPageTestimonialsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LandingPageTestimonialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
