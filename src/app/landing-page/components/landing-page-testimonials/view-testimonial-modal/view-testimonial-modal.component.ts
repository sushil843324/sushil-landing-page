import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-testimonial-modal',
  templateUrl: './view-testimonial-modal.component.html',
  styleUrls: ['./view-testimonial-modal.component.scss']
})
export class ViewTestimonialModalComponent implements OnInit {

  constructor() { }

  @Input() testimonial! : any

  ngOnInit(): void {
  }

}
