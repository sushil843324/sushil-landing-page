import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTestimonialModalComponent } from './view-testimonial-modal.component';

describe('ViewTestimonialModalComponent', () => {
  let component: ViewTestimonialModalComponent;
  let fixture: ComponentFixture<ViewTestimonialModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewTestimonialModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewTestimonialModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
