import { Component, Input, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewTestimonialModalComponent } from './view-testimonial-modal/view-testimonial-modal.component';



@Component({
  selector: 'app-landing-page-testimonials',
  templateUrl: './landing-page-testimonials.component.html',
  styleUrls: ['./landing-page-testimonials.component.scss']
})
export class LandingPageTestimonialsComponent implements OnInit {

  constructor(
    private modal : NgbModal
  ) { }

  isReadMore = false

  @Input() testimonialDetail : any

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 1500,
    autoplay : true,
    navText: ['', ''],
    margin : 10,
    nav: false,
    responsive: {
      0: {
        items: 1
      },
      576: {
        items: 2
      },
      768: {
        items: 3
      },
      992: {
        items: 3
      }
    },
  }

  ngOnInit(): void {
  }

  openModal(data:any) {
    const modal = this.modal.open(ViewTestimonialModalComponent)

    modal.componentInstance.testimonial = data; 
  }

}
