import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-landing-page-features',
  templateUrl: './landing-page-features.component.html',
  styleUrls: ['./landing-page-features.component.scss']
})
export class LandingPageFeaturesComponent implements OnInit {

  constructor() { }

  @Input() featureDetail : any

  ngOnInit(): void {
  }

}
