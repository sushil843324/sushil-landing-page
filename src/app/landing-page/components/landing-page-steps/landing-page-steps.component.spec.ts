import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPageStepsComponent } from './landing-page-steps.component';

describe('LandingPageStepsComponent', () => {
  let component: LandingPageStepsComponent;
  let fixture: ComponentFixture<LandingPageStepsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingPageStepsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LandingPageStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
