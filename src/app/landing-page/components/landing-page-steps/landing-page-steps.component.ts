import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page-steps',
  templateUrl: './landing-page-steps.component.html',
  styleUrls: ['./landing-page-steps.component.scss']
})
export class LandingPageStepsComponent implements OnInit {

  constructor() { }

  @Input() stepsDetail : any

  ngOnInit(): void {
  }

}
