import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPagePricingComponent } from './landing-page-pricing.component';

describe('LandingPagePricingComponent', () => {
  let component: LandingPagePricingComponent;
  let fixture: ComponentFixture<LandingPagePricingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingPagePricingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LandingPagePricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
