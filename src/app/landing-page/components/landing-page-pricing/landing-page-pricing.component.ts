import { Component, Input, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';


@Component({
  selector: 'app-landing-page-pricing',
  templateUrl: './landing-page-pricing.component.html',
  styleUrls: ['./landing-page-pricing.component.scss']
})
export class LandingPagePricingComponent implements OnInit {

  constructor() { }

  @Input() pricingDetail : any

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 1500,
    autoplay : true,
    navText: ['', ''],
    margin : 10,
    responsive: {
      0: {
        items: 1
      },
      576: {
        items: 2
      },
      768: {
        items: 3
      },
      992: {
        items: 3
      }
    },
    nav: false
  }

  ngOnInit(): void {
  }

}
