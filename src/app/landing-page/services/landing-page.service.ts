import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from '../interfaces/api-response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LandingPageService {

  constructor(
    private http : HttpClient
  ) { }

  // banner section

  getBannerSectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/banner-section/get`
    )
  }

  // feature section

  getFeatureSectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/feature-section/get`,
    )
  }

  // step section

  getStepSectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/steps-section/get`)
  }

  // job category section

  getJobCategorySectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/job-category-section/get`)
  }

  // pricing section

  getBudgetList() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/pricing-section/list`
    )
  }

  getPricingSectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/pricing-section/get`)
  }

  // testimonial section

  getTestimonialSectionData() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/testimonial-section/get`)
  }

  getAllTestimonials() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/feedbacks/list?isPublished=true`
    )
  }

  // contact card api

  addSupportMessage(data : any) {
    return this.http.post<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/support/add`,
      data
    )
  }

  // sliding logo section

  getSlidingLogos() {
    return this.http.get<ApiResponse>(
      `${environment.workforceUrl}/admin/landing-page/sliding-logo-section/get`
    )
  }

}
